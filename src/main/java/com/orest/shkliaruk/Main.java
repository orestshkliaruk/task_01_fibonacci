/**
 * Main
 * 23.01.2019
 * i have no copyright yet:(
 */
package com.orest.shkliaruk;

import java.util.Scanner;

/**
 * This is the Main class, that contains the program entry point
 * which performsCountig Fibonacci numbers, printing
 * out the even and odd numbers in specified order.
 *
 * @author orest.shkliaruk
 */

public class Main {

    public static void main(String[] args) {
        int[] userInterval;

        System.out.println("Type your interval");
        userInterval = new int[2];
        Scanner sc = new Scanner(System.in);
        userInterval[0] = sc.nextInt();
        userInterval[1] = sc.nextInt();
        System.out.println("You entered: " + userInterval[0] + " and "
                + userInterval[1]);
        FibonacciCounter fibonacciCounter = new FibonacciCounter();
        fibonacciCounter.countFibonacciForinterval(userInterval);
        if (!fibonacciCounter.arrOfFibinaccisForInterval.isEmpty()) {
            System.out.println("These are numbers of Fibonacci in your interval: ");
            fibonacciCounter.printFibonacciIntervalArray(fibonacciCounter
                    .arrOfFibinaccisForInterval);
        }
        OddOrEvenNumbersCounter oddOrEven = new OddOrEvenNumbersCounter();
        oddOrEven.printOddNumsWithSum(userInterval);
        oddOrEven.printEvenNumsWithSum(userInterval);
        fibonacciCounter.printPercentageFibonacci(fibonacciCounter
                .arrOfFibinaccisForInterval);
        oddOrEven.printBiggersOdEvenNum(fibonacciCounter
                .arrOfFibinaccisForInterval);
    }
}
