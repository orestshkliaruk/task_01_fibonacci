/**
 * OddOrEvenNumbersCounter
 * 23.01.2019
 * i have no copyright yet:(
 */
package com.orest.shkliaruk;

import java.util.ArrayList;

/**
 * OddOrEvenNumbersCounter Class contains 2 methods to work with the
 * interval entered by the user and
 * show all the odd\even munbers within entered interval.
 *  Also to print the sum of those numbers.
 */

public class OddOrEvenNumbersCounter {



    /**
     * Prints all the odd munbers within entered interval.
     * Prints the sum of those numbers.
     * @param userNumber the array of 2 integers, entered by user taken as
     *                   the interval to work with
     */
    public void printOddNumsWithSum(int[] userNumber) {
        int sumOdd = 0;

        System.out.println("Here are all Odd Numbers of your interval: ");
        for (int i = userNumber[0]; i <= userNumber[1]; i++) {
            if (i % 2 != 0) {
                System.out.print(i + " ");
            }
        }
        System.out.println("\nAlso, sum of all Odds is: ");
        for (int i = userNumber[0]; i <= userNumber[1]; i++) {
            if (i % 2 != 0) {
                sumOdd += i;
            }
        }
        System.out.println(sumOdd);
    }
    /**
     * Prints all the even munbers within entered interval.
     * Prints the sum of those numbers.
     * @param userNumber the array of 2 integers, entered by user taken as
     *                   the interval to work with
     */
    public void printEvenNumsWithSum(int[] userNumber) {
        int sumEven = 0;

        System.out.println("Here are all Even Numbers of your interval: ");
        for (int i = userNumber[1]; i >= userNumber[0]; i--) {
            if (i % 2 == 0)
                System.out.print(i + " ");
        }
        System.out.println("\nAlso, sum of all Evens is: ");
        for (int i = userNumber[1]; i >= userNumber[0]; i--) {
            if (i % 2 == 0)
                sumEven += i;
        }
        System.out.print(sumEven);
    }

    /**
     * print the biggest odd and even fibo number
      * @param arrOfFibinaccisForInterval interval to look for nums
     */
    public void printBiggersOdEvenNum(ArrayList<Integer>
                                              arrOfFibinaccisForInterval){
        for (int i = arrOfFibinaccisForInterval.size() - 1; i >1 ; i--){
            if ((arrOfFibinaccisForInterval.get(i) & 1 ) == 0){
                System.out.println("\nThis Fibonacci number is the biggest " +
                        "of even fibo numbers in your interval: ");
                System.out.println(arrOfFibinaccisForInterval.get(i));
                break;
            }
        }
        for (int i = arrOfFibinaccisForInterval.size(); i > 1; i--){
            if ((arrOfFibinaccisForInterval.get(arrOfFibinaccisForInterval
                    .size()-i)) % 2 != 0){
                System.out.println("\nThis Fibonacci number is the " +
                  "biggest of odd" + " fibo numbers in your interval: ");
                System.out.println(arrOfFibinaccisForInterval.get(i));
                break;
            }
        }

    }

}


