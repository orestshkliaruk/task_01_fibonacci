/**
 * FibonacciCounter
 * 23.01.2019
 * i have no copyright yet:(
 */
package com.orest.shkliaruk;

import java.util.ArrayList;
/**
 * This class is used to perform operations with
 * Fibonacci numbers for this task.
 */

public class FibonacciCounter {

    public ArrayList<Integer> arrOfFibinaccisForInterval =
            new ArrayList<Integer>();

    /**
     * Counts the Fibonacci munber of the entered digit.
      * @param userNumber - the number to count for
     * @return Fibonacci num of the userNumber
     */
    public static double countFibonacciNumber(int userNumber) {

        if (userNumber == 0) {
            return 0;
        }
        if (userNumber == 1) {
            return 1;
        } else {
            return countFibonacciNumber(userNumber - 1)
                    + countFibonacciNumber(userNumber - 2);
        }

    }

    /**
     * Counts he Fibo nums in the interval
     * @param userInterval interval to count in
     * @return an arraylist of fibo nums in the entered interval
     */
    public ArrayList countFibonacciForinterval(int[] userInterval) {

        for (int i = 0; i < userInterval[1]; i ++) {
            double kek = FibonacciCounter.countFibonacciNumber(i);
           // System.out.println(kek);
            if (kek > userInterval[0]) {
                if (kek < userInterval[1]) {
                    arrOfFibinaccisForInterval.add((int)kek);
                }
                if( kek>userInterval[1]){
                    break;
                }
            }

        }
        return arrOfFibinaccisForInterval;
    }

    /**
     * prints the fibo numers of the entered interval
     * @param arrOfFibinaccisForInterval entered interval
     */
    public void printFibonacciIntervalArray(ArrayList<Integer>
                                    arrOfFibinaccisForInterval){
        if(!arrOfFibinaccisForInterval.isEmpty()) {
                System.out.println(arrOfFibinaccisForInterval.toString());
        }else{
            System.out.println("WATAFAK");
        }
    }

    /**
     * counts and prints the persentage of odd\even fibos in
     * the specified interval
     * @param arrOfFibinaccisForInterval interval to count
     */
    public void printPercentageFibonacci(ArrayList<Integer>
                                                 arrOfFibinaccisForInterval) {
        double i = 0;
        double k = 0;
        double percentEven = 0;
        double percentOdd = 0;
        for (Integer n : arrOfFibinaccisForInterval) {
            if (n / 2 == 0) {
                i = i++;
            } else {
                i = k++;
            }
        }
        percentEven = 100 / (( i + k ) / k );
        percentOdd = 100 - percentEven;
        System.out.printf(" %n The persentage of odd to even is: "
                + "%.2f / %.2f ", percentOdd, percentEven);
    }



}
